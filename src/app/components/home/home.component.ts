import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  realeses: any[] = [];
  loading: boolean;

  constructor(private spotify: SpotifyService) {
    this.loading = true;
    this.spotify.getNewRealeses().subscribe(
      (realeses: any) => {
        console.log(realeses);
        this.realeses = realeses;
        this.loading = false;
      }
    )
  }


  ngOnInit() {
  }

  

}
