import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: []
})
export class ArtistaComponent implements OnInit {

  artistId: string;

  constructor(private aRoute: ActivatedRoute) {
    this.aRoute.params.subscribe(
      params => {
        this.artistId = params.id;
      }
    )
   }

  ngOnInit() {
  }

}
