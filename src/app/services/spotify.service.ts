import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) { }

  getQuery(query){
    const url = 'https://api.spotify.com/v1/';

    const headers = new HttpHeaders({
      Authorization: 'Bearer BQAFd1wbbxbSHhh1BD-qyTAQ9IFrfsOmD2Yjq83JWSTwrSmEr8f9JLaVCyrVRGIM9Z9X4P7FKDer7oAzOAc'
    })

    return this.http.get(url + query, { headers })

  }

  getNewRealeses(){
    return this.getQuery('browse/new-releases')
      .pipe( map( data => {
        return data['albums'].items
      } ) )
  }

  getArtista(termino: string){

    return this.getQuery(`search?q=${ termino }&type=artist&limit=15`)
      .pipe( map( data => {
        return data['artists'].items
      } ) )
  }

}
